tunics -- Tom's UNIX
====================

Rewriting super-simple versions of classic UNIX tools to learn C.

License
-------

Copyright (c) [Tom Ryder][1]. Distributed under an [MIT License][2].

[1]: https://sanctum.geek.nz/
[2]: https://www.opensource.org/licenses/MIT
