#ifndef __SORT_H
#define __SORT_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h> 

#define VEC_SIZE 64
#define BUF_SIZE 64

typedef struct {
    unsigned long count, size;
    char **data;
} vec;

char *read_line(FILE*);
vec *vec_init(void);
void vec_add(vec*, char*);
void vec_free(vec*);
static int qsort_strcmp(const void*, const void*);

#endif
