#include "whoami.h"

int main(void) {
    struct passwd *p;

    if (!(p = getpwuid(getuid()))) {
        exit(EXIT_FAILURE);
    }

    printf("%s\n", p->pw_name);

    exit(EXIT_SUCCESS);
}
