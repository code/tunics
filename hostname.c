#include "hostname.h"

int main(void) {
    struct utsname *name;
    name = malloc(sizeof(struct utsname));
    if (uname(name) == -1) {
        perror("uname");
        exit(EXIT_FAILURE);
    }
    fprintf(stdout, "%s\n", name->nodename);
    exit(EXIT_SUCCESS);
}
