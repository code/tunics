#include "pwd.h"

int main(void)
{
    char buf[PATH_MAX];
    if (!getcwd(buf, sizeof(buf))) {
        perror("getcwd");
        exit(EXIT_FAILURE);
    }
    fprintf(stdout, "%s\n", buf);
    exit(EXIT_SUCCESS);
}
