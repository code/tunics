.PHONY: all clean

CC = clang
CFLAGS = -std=c90 -Weverything

all : hostname ls pwd sort whoami

hostname : hostname.c hostname.h
	$(CC) $(CFLAGS) hostname.c -o hostname

ls : ls.c ls.h
	$(CC) $(CFLAGS) ls.c -o ls

pwd : pwd.c pwd.h
	$(CC) $(CFLAGS) pwd.c -o pwd

sort : sort.c sort.h
	$(CC) $(CFLAGS) sort.c -o sort

whoami : whoami.c whoami.h
	$(CC) $(CFLAGS) whoami.c -o whoami

clean :
	rm -f -- *.o
	rm -f hostname ls pwd sort whoami
