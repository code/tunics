#include "ls.h"

int main(int argc, char **argv)
{
    char *dirname;
    DIR *dir;
    struct dirent *dirent;

    if (argc > 1) {
        dirname = argv[1];
    } else {
        dirname = ".";
    }

    if (!(dir = opendir(dirname))) {
        perror("opendir");
        free(dir);
        exit(EXIT_FAILURE);
    }

    while ((dirent = readdir(dir))) {
        fprintf(stdout, "%s\n", dirent->d_name);
    }
    if (errno) {
        perror("readdir");
        free(dir);
        free(dirent);
        exit(EXIT_FAILURE);
    }

    free(dir);
    free(dirent);
    exit(EXIT_SUCCESS);
}
